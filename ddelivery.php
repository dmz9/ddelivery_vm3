<?php


defined( '_JEXEC' ) or die( 'Restricted access' );

if ( ! class_exists( 'vmPSPlugin' ) ) {
	require( JPATH_VM_PLUGINS . DS . 'vmpsplugin.php' );
}
jimport( 'joomla.plugin.plugin' );

include_once "vendor/autoload.php";
include_once "plgVmShipmentDdelivery.php";
