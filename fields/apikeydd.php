<?php

use Ipol\Joomla\Virtuemart3\Helper;

defined( 'JPATH_BASE' ) or die();
JFormHelper::loadFieldClass( 'text' );
jimport( 'joomla.form.formfield' );

class JFormFieldapikeydd extends \JFormFieldText {
	var $type = 'apikeydd';
	
	/**
	 * метод для рендера поля апи ключа
	 * кроме прочего - проверку на корректность ключа делаем тут.
	 * ссылку на личный кабинет дописыавем сюда же.
	 * инициализация базы для хранения всей шелухи - тоже тут.
	 *
	 * @return string
	 */
	protected function getInput() {
		$cabinetLink = '';
		if ( ! empty( $this->value ) ) {
			$container = Helper::createContainer();
			try {
				/**
				 * @var $ddeliveryEndpoint string точка входа для сервера дделивери
				 */
				$ddeliveryEndpoint = \Ipol\Joomla\Virtuemart3\Router::buildActionUrl( 'admin' );
				$business          = $container->getBusiness();
				/** инициализация хранилища в sqlite */
				$business->initStorage();
				/** получаем токен для проверки корректности ключа */
				/** в этот момент происходит привязка конечной точки магазина к дделивери ключу */
				$token = $business->renderAdmin( $ddeliveryEndpoint );
				if ( empty( $token ) ) {
					throw new \Exception( 'Пустой токен получен от DDelivery. Проверьте корректность ключа.' );
				}
				$cabinetLink = "<p>Ключ привязан! <a href='$ddeliveryEndpoint' target='_blank'>Перейти в настройки</a></p>";
			} catch ( \Exception $exception ) {
				echo "<p>Ошибка: {$exception->getMessage()}</p>";
			}
		}
		
		return parent::getInput() . $cabinetLink;
	}
}