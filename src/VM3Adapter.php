<?php

namespace Ipol\Joomla\Virtuemart3;

use DDelivery\Adapter\Adapter;

class VM3Adapter extends Adapter {
	/**
	 * метод получает адрес файла базы данных sqlite
	 *
	 * @return string
	 */
	public function getPathByDB() {
		$dir = dirname( __FILE__ );
		
		return implode( DIRECTORY_SEPARATOR,
		                array(
			                dirname( $dir ),
			                'db',
			                'db.sqlite'
		                ) );
	}
	
	/**
	 *
	 * Получить апи ключ из настроек системы
	 *
	 * @return string
	 */
	public function getApiKey() {
		return Helper::getConfig('api');
	}
	
	/**
	 *
	 * При синхронизации статусов заказов необходимо
	 * [
	 *      'id' => 'status',
	 *      'id2' => 'status2',
	 * ]
	 *
	 * @param array $orders
	 *
	 * @return bool
	 */
	public function changeStatus( array $orders ) {
		// TODO: Implement changeStatus() method.
	}
	
	public function getCmsName() {
		return 'Joomla VM3';
	}
	
	public function getCmsVersion() {
		return '3';
	}
	
	/**
	 * Получить  заказ по id
	 * ['city' => город назначения, 'payment' => тип оплаты, 'status' => статус заказа,
	 * 'sum' => сумма заказа, 'delivery' => стоимость доставки]
	 *
	 * город назначения, тип оплаты, сумма заказа, стоимость доставки
	 *
	 * @param $id
	 *
	 * @return array
	 */
	public function getOrder( $id ) {
		// TODO: Implement getOrder() method.
	}
	
	/**
	 * Получить список заказов за период
	 * ['city' => город назначения, 'payment' => тип оплаты, 'status' => 'статус заказа'
	 * 'sum' => сумма заказа, 'delivery' => стоимость доставки]
	 *
	 * город назначения, тип оплаты, сумма заказа, стоимость доставки
	 *
	 * @param $from
	 * @param $to
	 *
	 * @return array
	 */
	public function getOrders( $from, $to ) {
		// TODO: Implement getOrders() method.
	}
	
	/**
	 *
	 * Получить скидку
	 *
	 * @return float
	 */
	public function getDiscount() {
		return 0;
	}
	
	/**
	 *
	 * получить продукты из корзины
	 *
	 * @return array
	 */
	public function getProductCart() {
		$products = array();
		if ( ! class_exists( 'VirtueMartCart' ) ) {
			require( JPATH_VM_SITE . DS . 'helpers' . DS . 'cart.php' );
		}
		
		$cart = \VirtueMartCart::getCart();
		$cart->setCartIntoSession();
		$cart->prepareCartData();
		$prods = $cart->products;
		
		if ( is_array( $prods ) && count( $prods ) ) {
			foreach ( $prods as $prod ) {
				$width  = ( (double) \ShopFunctions::convertDimensionUnit( (double) $prod->product_width,
				                                                           $prod->product_lwh_uom,
				                                                           'CM' ) > 1 )
					? \ShopFunctions::convertDimensionUnit( (double) $prod->product_width,
					                                        $prod->product_lwh_uom,
					                                        'CM' ) : 10;
				$height = ( (double) \ShopFunctions::convertDimensionUnit( (double) $prod->product_height,
				                                                           $prod->product_lwh_uom,
				                                                           'CM' ) >= 1 )
					? \ShopFunctions::convertDimensionUnit( (double) $prod->product_height,
					                                        $prod->product_lwh_uom,
					                                        'CM' ) : 10;
				$length = ( (double) \ShopFunctions::convertDimensionUnit( (double) $prod->product_length,
				                                                           $prod->product_lwh_uom,
				                                                           'CM' ) >= 1 )
					? \ShopFunctions::convertDimensionUnit( (double) $prod->product_length,
					                                        $prod->product_lwh_uom,
					                                        'CM' ) : 10;
				$weight = ( (double) \ShopFunctions::convertWeigthUnit( (double) $prod->product_weight,
				                                                        $prod->product_weight_uom,
				                                                        'KG' ) > 0 )
					? \ShopFunctions::convertWeigthUnit( (double) $prod->product_weight,
					                                     $prod->product_weight_uom,
					                                     'KG' ) : 1;
				$price  = round((float)$prod->prices['subtotal'],2);
				$sku    = ( $prod->product_sku ) ? $prod->product_sku : $prod->virtuemart_product_id;
				
				$products[ $prod->virtuemart_product_id ] = [
					'id'       => $prod->virtuemart_product_id,
					'sku'      => $sku,
					'name'     => $prod->product_name,
					'price'    => $price,
					'quantity' => $prod->quantity,
					'width'    => $width,
					'height'   => $height,
					'length'   => $length,
					'weight'   => $weight
				];
			}
		}
		
		return $products;
	}
	
	/**
	 * Получить массив с соответствием статусов DDelivery
	 *
	 * @return array
	 */
	public function getCmsOrderStatusList() {
		// TODO: Implement getCmsOrderStatusList() method.
	}
	
	/**
	 * Получить массив со способами оплаты
	 *
	 * @return array
	 */
	public function getCmsPaymentList() {
		// TODO: Implement getCmsPaymentList() method.
	}
	
	/***
	 *
	 * В этом участке средствами Cms проверить права доступа текущего пользователя,
	 * это важно так как на базе этого  метода происходит вход
	 * на серверние настройки
	 *
	 * @return bool
	 */
	public function isAdmin() {
		return true;
	}
	
	/**
	 * метод получает реальную точку входа в цмс
	 * если оставить как есть пустым - будет считаться тот урл с которого обращаются
	 * часто это не тот адрес который нужен
	 * участвует когда происходит привязка ключа к магазину
	 *
	 * @return string
	 */
	public function getEnterPoint() {
		return Router::buildActionUrl();
	}
}