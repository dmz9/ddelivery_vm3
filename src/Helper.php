<?php
/**
 * Created by PhpStorm.
 * User: x
 * Date: 14.09.2017
 * Time: 13:07
 */

namespace Ipol\Joomla\Virtuemart3;

use DDelivery\Adapter\Container;

class Helper {
	public static function createContainer( array $adapterParams = array() ) {
		$adapter = new VM3Adapter( $adapterParams );
		
		return new Container( [ 'adapter' => $adapter ] );
	}
	
	/**
	 * получаем настройки прямо из бд.
	 *
	 * @param      $param
	 * @param null $default
	 *
	 * @return mixed|null
	 */
	public static function getConfig( $param, $default = null ) {
		$db = \JFactory::getDbo();
		$db->setQuery( "select shipment_params from #__virtuemart_shipmentmethods where shipment_element = 'ddelivery'" );
		$result = $db->loadResult();
		$params = explode( '|', $result );
		$result = array();
		if ( count( $params ) ) {
			foreach ( $params as $k => $v ) {
				if ( strpos( $v, '=' ) !== false ) {
					$t = explode( '=', $v );
					if ( count( $t ) == 2 ) {
						$result[ $t[ 0 ] ] = json_decode( $t[ 1 ], true );
					}
				}
			}
		}
		
		return ( isset( $result[ $param ] ) ) ? $result[ $param ] : $default;
	}
}