<?php
/**
 * Created by PhpStorm.
 * User: x
 * Date: 14.09.2017
 * Time: 18:37
 */

namespace Ipol\Joomla\Virtuemart3;

use DDelivery\Adapter\Container;

class EndpointController {
	/**
	 * @var array $_REQUEST
	 */
	protected $request;

	public function __construct( $request, Container $container ) {
		$this->request = $request;
	}

	private function _methods() {
		return array( 'getUserCart', 'generateSDKToken', 'savePrice', 'saveSDK' );
	}

	/**
	 * @param $action
	 *
	 * @return string
	 */
	public function handle( $action ) {
		if ( in_array( $action,
		               $this->_methods() ) ) {
			$method = "action" . ucfirst( $action );

			echo json_encode( $this->$method() );
		} else {
			Helper::createContainer()
			      ->getUi()
			      ->render( $this->request );
		}

		return '';
	}

	public function actionGenerateSDKToken() {
		$products = $this->request['products'];
		$discount = $this->request['discount'];

		$container = Helper::createContainer( array(
			                                      'form'     => $products,
			                                      'discount' => (float) $discount
		                                      ) );
		$business  = $container->getBusiness();

		$cartAndDiscount = $container->getAdapter()
		                             ->getCartAndDiscount();
		$token           = $business->renderModuleToken( $cartAndDiscount );

		return array(
			'url' => $container->getAdapter()
			                   ->getSdkServer() . 'delivery/' . $token . '/index.json'
		);
	}

	public function actionGetUserCart() {
		$container = Helper::createContainer();

		return $container->getAdapter()
		                 ->getProductCart();
	}

	public function actionSavePrice(  ) {
		$session = \JFactory::getSession();

		$session->set( 'cost',
		               $this->request['price'],
		               'ddelivery' );

		return array( 'success' => 1, 'status' => 'ok' );
	}

	public function actionSaveSDK(  ) {
		$session = \JFactory::getSession();
		$session->set( 'sdk',
		               $this->request['sdk'],
		               'ddelivery' );

		return array( 'success' => 1, 'status' => 'ok' );
	}
}