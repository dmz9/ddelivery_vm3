<?php
/**
 * Created by PhpStorm.
 * User: x
 * Date: 14.09.2017
 * Time: 17:49
 */

namespace Ipol\Joomla\Virtuemart3;

class Router {
	static $baseUrl = 'index.php?option=com_virtuemart&view=plugin&type=vmshipment&name=ddelivery&format=json';

	public static function buildActionUrl( $action = '' ) {
		$url = \JUri::root()  . self::$baseUrl;
		if ( ! empty( $action ) ) {
			$url .= "&action=$action";
		}

		return $url;
	}
}